Rails.application.routes.draw do

  get '/login', to: 'tops#login'
  post '/login', to: 'tops#create'
  delete '/logout', to: 'tops#destroy'
  get '/tops/main'
  root to: 'users#index'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
