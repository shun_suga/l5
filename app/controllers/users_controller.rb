class UsersController < ApplicationController

  def index
    @persons = Person.all
  end

  def new
    @person = Person.new
  end

  def create
    @person = Person.new(user_params)

    if @person.save
      redirect_to new_user_path, notice: "ユーザーを登録しました。"
    else
      render :new
    end
  end

  private

  def user_params
    params.require(:person).permit(:uid, :password)
  end


end
