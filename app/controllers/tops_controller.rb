class TopsController < ApplicationController
  def main
  end
  def login
  end

  def create
    @user = Person.find_by(uid: session_params[:uid])

    if @user&.authenticate(session_params[:password])
      session[:user_id] = @user.id
      redirect_to tops_main_url, notice: 'ログインしました。'
    else
      render :login
    end
  end

  def destroy
    reset_session
    redirect_to root_url, notice: 'ログアウトしました。'
  end

  private
  def session_params
    params.require(:session).permit(:uid, :password)
  end

end
